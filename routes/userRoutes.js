
const express = require('express');
//Create routes

//Router() handles the requests
const router = express.Router();

//Syntax: router.HTTPmethod(`url`, <request listener>)
router.get('/', (req, res) => {
	//console.log(`Hello from userRoutes`);
	res.send(`Hello from userRoute`);
});

module.exports = router;