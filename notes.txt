1.) initialize node package manager (NPM)
	npm init

2.) express.js is a 3rd part library (framework)
	//install express.js package
	npm i express

	//require express module in the project


3.) mongoose is a package needs to be installed to be used to  interact with a server
	//reference https://mongoosejs.com/docs/5.x/docs/guide.html#
	npm i mongoose

	//require mongoose module in the project

4.) Connect your database in the project
	//get connection string from mongoDB atlas
	//use connect () method
		//update the password of the connection string
		//change the name of the database in the connection string

5.) Optional: Add notification of wheter database is connected

6.) Create a Schema

7.) Create a model out of the Schema
		// why? to make use of Model methods
			// findById()
			// findOne()

8.) Create a route
	//Note: add parser

	properties of req:
1. method
2. params
3. body
4. headers

		// use Router() to handle requests (userRoutes)
			// Syntax:
				router.HTTPmethod('url',<request listener>)

9.) Create .gitignore file
		//so that when you push your project in your online repository, other modules not necessary for the project will not be pushed
		
		touch .gitignore